using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersShortResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var result = new CustomersShortResponse();
            result.Customers.AddRange(response);

            return result;
        }

        public override async Task<CustomerResponse> GetCustomer(EntityId request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };
            response.Preferences?.AddRange(customer.Preferences.Select(cp => new PreferenceResponse()
            {
                Id = cp.PreferenceId.ToString(),
                Name = cp.Preference.Name
            }));

            return response;
        }

        public override async Task<EntityId> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(pId => Guid.Parse(pId)).ToList());

            var newId = Guid.NewGuid();

            var customer = new Customer()
            {
                Id = newId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = newId,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return new EntityId { Id = customer.Id.ToString() };
        }

        public override async Task<Empty> EditCustomers(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id.ToString()));

            if (customer == null)
                throw new Exception($"Customer {request.Id} not found.");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(
                request.PreferenceIds.Select(id => Guid.Parse(id)).ToList());

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(p => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = p,
                PreferenceId = p.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(EntityId request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception($"Customer {request.Id} not found.");

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}