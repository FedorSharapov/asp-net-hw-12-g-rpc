﻿using Grpc.Net.Client;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GrpcCustomersClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:6001");
            /*var client = new GrpcCustomersClient.CustomerClient(channel);
            var reply = await client.SayHelloAsync(
                              new GetCustomersAsync());*/
            Console.WriteLine($"");
            Console.WriteLine("Press any key to exit...");
            Thread.Sleep(3000);
        }
    }
}
